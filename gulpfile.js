var gulp = require('gulp'),
  connect = require('gulp-connect'),
  htmlImport = require('gulp-html-import'),
  htmlmin = require('gulp-html-minifier');

gulp.task('connect', function () {
	connect.server({
		root: './', //path to project
		livereload: true,
	});
});

gulp.task('import', function () {
    gulp.src('src/img/*').pipe(gulp.dest('dist/img'));
    gulp.src('./src/*.html')
        .pipe(htmlImport('./src/'))
        .pipe(htmlmin({collapseWhitespace: true}))
        .on('error', hendlerError)
        .pipe(gulp.dest('dist'))
        .pipe(connect.reload());
})

function hendlerError (error) {
  // If you want details of the error in the console
  console.log(error.toString())
  this.emit('end')
}

gulp.on('error', function(error){
  console.log(error);
});

// watch
gulp.task('watch', function () {
	gulp.watch('**/*.html', ['import']);
});

gulp.task('default', ['connect', 'import', 'watch']);